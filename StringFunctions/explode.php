<?php
/**
explode — Split a string by string
 * Description
array explode ( string $delimiter , string $string [, int $limit = PHP_INT_MAX ] )
 */


// Example
$egg  = "piece1,piece2,piece3,piece4,piece5,piece6";
$pieces = explode(",", $egg);
echo $pieces[0]; // piece1
echo "<hr>";
echo $pieces[1]; // piece2
echo "<hr>";
echo $pieces[2]; // piece3
echo "<hr>";
echo $pieces[3]; // piece4
?>