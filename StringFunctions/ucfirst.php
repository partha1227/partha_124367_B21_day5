<?php
/*
 *
 * ucfirst
 *
 * ucfirst — Make a string's first character uppercase
 *
 *
 *
Description
string ucfirst ( string $str )

Returns a string with the first character of str capitalized, if that character is alphabetic.

Note that 'alphabetic' is determined by the current locale. For instance, in the default "C" locale characters such as umlaut-a (ä) will not be converted.
Parameters

str

    The input string.

Return Values

Returns the resulting string.

 */



$foo = 'partha protim paul';
$foo = ucfirst($foo);             

$bar = 'PARTHA PROTIM PAUL';
$bar = ucfirst($bar);             
$bar = ucfirst(strtolower($bar)); 

echo $foo;
echo "<hr>";
echo $bar;
?>

