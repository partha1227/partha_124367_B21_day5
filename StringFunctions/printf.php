<?php
/**
printf — Output a formatted string
 *
Description
int printf ( string $format [, mixed $args [, mixed $... ]] )

Produces output according to format.
Parameters

format

See sprintf() for a description of format.
args
...

Return Values

Returns the length of the outputted string.

 */


$number = 3;
$str = "Chittagong";
printf("There are %u million byke in %s.",$number,$str);
?>