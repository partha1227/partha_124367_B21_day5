<?php
/**
 *addslashes — Quote string with slashes
 *
 * Description
string addslashes ( string $str )
 */

$str = "Is your name P'Partha?";

// Outputs: Is your name P\'Partha?
echo addslashes($str);