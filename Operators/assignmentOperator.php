<?php
/*
Assignment Operators

The basic assignment operator is "=". Your first inclination might be to think of this as "equal to". Don't. It really means that the left operand gets set to the value of the expression on the right (that is, "gets set to").

The value of an assignment expression is the value assigned. That is, the value of "$a = 3" is 3. This allows you to do some tricky things:

*/


$a = 42;
$b = 20;

$c = $a + $b;   /* Assignment operator */
echo "Addtion Operation Result: $c <br/>";

$c += $a;  /* c value was 42 + 20 = 62 */
echo "Add AND Assigment Operation Result: $c <br/>";

$c -= $a; /* c value was 42 + 20 + 42 = 104 */
echo "Subtract AND Assignment Operation Result: $c <br/>";

$c *= $a; /* c value was 104 - 42 = 62 */
echo "Multiply AND Assignment Operation Result: $c <br/>";

$c /= $a;  /* c value was 62 * 42 = 2604 */
echo "Division AND Assignment Operation Result: $c <br/>";

$c %= $a; /* c value was 2604/42 = 62*/
echo "Modulus AND Assignment Operation Result: $c <br/>";


?>